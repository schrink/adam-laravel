<?php

return [
    'url' => env('CANVAS_URL'),
    'access_token' => env('CANVAS_ACCESS_TOKEN')
];