<?php namespace App\Adam\Canvas;


class Dates {

    protected $course_id;

    public function __construct($course_id) {
        $this->$course_id = $course_id;
    }

    public function getStartDate() {
        return Canvas::start($this->$course_id);
    }


}