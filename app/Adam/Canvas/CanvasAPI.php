<?php namespace App\Adam\Canvas;

use GuzzleHttp\Client;
use Illuminate\Http\Response;

class CanvasAPI
{

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function courses()
    {
        $response = $this->client->get('courses');

        return $response->json();

    }

    public function course($id)
    {
        $response = $this->client->get("courses/$id");

        return $response->json();
    }

    public function modules($course_id)
    {
        $response = $this->client->get("courses/$course_id/modules?include=items?per_page=100");

        return $response->json();
    }

    public function assignments($course_id)
    {
        $response = $this->client->get("courses/$course_id/assignments?per_page=100");

        return $response->json();
    }

    public function topics($course_id, $module_id)
    {
        $response = $this->client->get("courses/$course_id/modules/$module_id/items?per_page=100");

        return $response->json();
    }

    public function teachers($course_id)
    {
        $response = $this->client->get("courses/$course_id/users?enrollment_type=teacher");

        return $response->json();
    }

    public function students($course_id)
    {
        $response = $this->client->get("courses/$course_id/users?enrollment_type=student&per_page=100&include=avatar_url");

        return $response->json();
    }

    public function groups($account_id = 1)
    {

        $groups = $this->client->get("accounts/1/groups?per_page=100")->json();
        $response = [];
        foreach ($groups as $key => $group) {

            $response[$group['id']] = $group;
        }

        return $response;
    }

    public function user($id)
    {
        $response = $this->client->get("users/$id/profile");

        return $response->json();
    }

    public function allTeachers()
    {
        $teachers = $this->client->get("groups/72/users?per_page=100")->json();

        $response = [];
        foreach ($teachers as $key => $teacher) {

            $response[$teacher['id']] = $teacher;
        }
        /*$courses = $this->client->get("accounts/1/courses?per_page=100")->json();

        foreach($courses as $course) {
            $user = $this->teachers($course['id']);
            dd($user);
            $response[$user['id']] = $user;
        }*/
        return $response;

    }

    public function createCallendarEvent($options)
    {
        $response = $this->client->post("calendar_events", ['body' => $options]);
        return $response->json();
    }

    public function updateCallendarEvent($id, $options)
    {
        $response = $this->client->put("calendar_events/$id", ['body' => $options]);
        return $response->json();
    }

    public function deleteCallendarEvent($event_id)
    {
        $response = $this->client->delete("calendar_events/$event_id");
        return $response->json();
    }

    public function classroomJson()
    {

    }


}