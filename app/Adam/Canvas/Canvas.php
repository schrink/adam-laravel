<?php namespace App\Adam\Canvas;

use Illuminate\Support\Facades\Facade;

class Canvas extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'canvas';
    }
}
