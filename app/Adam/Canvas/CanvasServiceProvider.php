<?php namespace App\Adam\Canvas;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client;

class CanvasServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('canvas', function()
        {
            $client = new Client([
                'base_url' => Config::get('canvas.url') . 'api/v1/',
                'defaults' => [
                    'headers' => [
                        'Authorization' => 'Bearer ' . Config::get('canvas.access_token'),
                        'Accept' => 'application/json'
                    ],
                ]
            ]);

            return new CanvasAPI($client);
        });

    }
}